#include "DsoNode.h"

#include <cv_bridge/cv_bridge.h>


#include <IOWrapper/OutputWrapper/SampleOutputWrapper.h>
#include <IOWrapper/Pangolin/PangolinDSOViewer.h> 
#include <util/settings.h>


DsoNode::DsoNode()
  : it(n)
  , pub(n.advertise<sensor_msgs::PointCloud2>("/cloud", 1))
  , sub(it.subscribe("/image_raw", 1, &DsoNode::callback, this))
  , rate(ros::Rate(10))
  , frame_id(0)
  , full_system(0)
  , undistorter(0)
{}


void DsoNode::init(std::string calib)
{
  dso::setting_desiredImmatureDensity = 1000;
	dso::setting_desiredPointDensity = 1200;
	dso::setting_minFrames = 5;
	dso::setting_maxFrames = 7;
	dso::setting_maxOptIterations=4;
	dso::setting_minOptIterations=1;
	dso::setting_logStuff = false;
	dso::setting_kfGlobalWeight = 1.3;
  dso::setting_photometricCalibration = 2;
	dso::setting_affineOptModeA = 0;
	dso::setting_affineOptModeB = 0;

  undistorter = dso::Undistort::getUndistorterForFile(calib, "", "");

  dso::setGlobalCalib(
      (int)undistorter->getSize()[0],
      (int)undistorter->getSize()[1],
      undistorter->getK().cast<float>());
  
  full_system = new dso::FullSystem();
  full_system->linearizeOperation=false;

  full_system->outputWrapper.push_back(new dso::IOWrap::PangolinDSOViewer(
      (int)undistorter->getSize()[0],
      (int)undistorter->getSize()[1]));
  full_system->outputWrapper.push_back(new dso::IOWrap::SampleOutputWrapper());
}

void DsoNode::callback(const sensor_msgs::ImageConstPtr& img)
{
  auto cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
  assert(cv_ptr->image.type() == CV_8U);
	assert(cv_ptr->image.channels() == 1);

  if(dso::setting_fullResetRequested)
  {
    auto wraps = full_system->outputWrapper;
    delete full_system;
    for(auto const& ow : wraps) ow->reset();
    full_system = new dso::FullSystem();
    full_system->linearizeOperation = false;
    full_system->outputWrapper = wraps;
  }

  dso::MinimalImageB min_img(cv_ptr->image.cols, cv_ptr->image.rows, (unsigned char*) cv_ptr->image.data);
  dso::ImageAndExposure* undist_img = undistorter->undistort<unsigned char>(&min_img, 1,0, 1.0f);
  undist_img->timestamp=img->header.stamp.toSec(); // relay the timestamp to dso
  full_system->addActiveFrame(undist_img, frame_id);
  frame_id++;
  delete undist_img;
}


void DsoNode::spin()
{
  full_system->printResult("result.txt"); 
  for(dso::IOWrap::Output3DWrapper* ow : full_system->outputWrapper)
  {
      ow->join();
      delete ow;
  }

  while(ros::ok())
  {
    pub.publish(pub_val);
    ros::spinOnce();
    rate.sleep();
  }
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "dso_node");

  DsoNode dso_node = DsoNode();
  std::string calib = "/home/uvr-user/Development/catkin_ws/calib_dso/calib_zed_hd.txt";

  // dso::setting_desiredImmatureDensity = 1000;
	// dso::setting_desiredPointDensity = 1200;
	// dso::setting_minFrames = 5;
	// dso::setting_maxFrames = 7;
	// dso::setting_maxOptIterations=4;
	// dso::setting_minOptIterations=1;
	// dso::setting_logStuff = false;
	// dso::setting_kfGlobalWeight = 1.3;


	// printf("MODE WITH CALIBRATION, but without exposure times!\n");
	// dso::setting_photometricCalibration = 2;
	// dso::setting_affineOptModeA = 0;
	// dso::setting_affineOptModeB = 0;

  // dso::Undistort* undistorter = dso::Undistort::getUndistorterForFile(calib, "", "");

  // dso::setGlobalCalib(
  //         (int)undistorter->getSize()[0],
  //         (int)undistorter->getSize()[1],
  //         undistorter->getK().cast<float>());

  // dso::FullSystem* fullSystem = new dso::FullSystem();
  // fullSystem->linearizeOperation=false;

  dso_node.init(calib);

  dso::setting_fullResetRequested=true;

  dso_node.spin();

  return 0;
}
